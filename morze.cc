#include <stdio.h>
#include <cstdlib>
#include <iostream>
#include <mpi.h>
#include <vector>
#include <unistd.h>

#define FIELDNO 4
#define WEZ_POJAZD 1
#define ODDAJ_POJAZD 2
#define WEZ_MECH 3
#define ODDAJ_MECH 4
#define WEZ_MORZE 5
#define ODDAJ_MORZE 6
#define KOLEJKA_POJAZD 7
#define KOLEJKA_MORZE 8
#define KOLEJKA_MECH 9
#define OK 10

using namespace std;

pthread_t threadReciver, threadMain;
void *reciveFunc(void *ptr);
void *mainFunc(void *ptr);
MPI_Datatype MPI_PAKIET_T;

int space_on_sea;
int vehicles;
int mechanics;
int Lclock;
int max_state;
int size;
int ranking;
int ok;
int poss;
char list_c;
bool ending;

pthread_mutex_t vehicles_mut = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t sea_mut = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t mechanics_mut = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t ok_mut = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t clock_mut = PTHREAD_MUTEX_INITIALIZER;

struct tableInt
{
    int key;
    int value;
};

vector<tableInt> vehicle_table;
vector<tableInt> vehicle_list;
vector<tableInt> mechanic_list;
vector<tableInt> sea_list;

typedef struct
{
    int ts;
    int data;
    int dst;
    int src;
} packet_t;

void wait()
{
    int time = rand() % 3 + 2;
    sleep(3);
}

void inicjuj(int *argc, char ***argv)
{

    ending = false;
    int provided;
    MPI_Init_thread(argc, argv, MPI_THREAD_MULTIPLE, &provided);
    

    const int nitems = FIELDNO;
    int blocklengths[FIELDNO] = {1, 1, 1, 1};
    MPI_Datatype typy[FIELDNO] = {MPI_INT, MPI_INT, MPI_INT, MPI_INT};
    MPI_Aint offsets[FIELDNO];

    offsets[0] = offsetof(packet_t, ts);
    offsets[1] = offsetof(packet_t, data);
    offsets[2] = offsetof(packet_t, dst);
    offsets[3] = offsetof(packet_t, src);

    MPI_Type_create_struct(nitems, blocklengths, offsets, typy, &MPI_PAKIET_T);
    MPI_Type_commit(&MPI_PAKIET_T);

    MPI_Comm_rank(MPI_COMM_WORLD, &ranking);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    srand(ranking);

    vehicles = 4;
    space_on_sea = 4;
    mechanics = 2;
    max_state = 1;

    vehicle_table.reserve(vehicles);
    vehicle_list.reserve(size);
    sea_list.reserve(size);
    mechanic_list.reserve(size);

    for (int i = 0; i < vehicles; i++)
    {
        tableInt tmp;
        tmp.key = 1;
        tmp.value = max_state;
        vehicle_table.push_back(tmp);
    }

    Lclock = 0;
    list_c = '0';

    pthread_create(&threadReciver, NULL, reciveFunc, 0);
    pthread_create(&threadMain, NULL, mainFunc, 0);
}

vector<tableInt> add_too_vec(vector<tableInt> list, int K, int V)
{
    vector<tableInt> end;
    tableInt tmp;
    tmp.key = K;
    tmp.value = V;
    int state = 0;
    if (list.empty())
    {
        end.push_back(tmp);
    }
    else
    {
        for (unsigned int i = 0; i < list.size(); ++i)
        {
            if (V < list[i].value && state == 0)
            {
                end.push_back(tmp);
                state = 1;
            }
            if (V == list[i].value && K < list[i].key && state == 0)
            {
                end.push_back(tmp);
                state = 1;
            }
            end.push_back(list[i]);
        }
        if (state == 0)
        {
            end.push_back(tmp);
        }
    }
    return end;
}

int vec_poss(vector<tableInt> list, int K)
{
    int R = -1;
    for (unsigned int i = 0; i < list.size(); ++i)
    {
        if (list[i].key == K)
        {
            R = i;
        }
    }
    return R;
}

void clock_max(int C)
{
    pthread_mutex_lock(&clock_mut);
    if (C == -1)
        C = Lclock + 1;
    Lclock = max(Lclock, C);
    pthread_mutex_unlock(&clock_mut);
}

void *reciveFunc(void *ptr)
{
    packet_t pakiet;
    pakiet.src = ranking;
    MPI_Status status;
    int S_poss;
    while (!ending)
    {
        MPI_Recv(&pakiet, 1, MPI_PAKIET_T, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
        pakiet.dst = status.MPI_SOURCE;
        clock_max(pakiet.ts);
        switch ((int)status.MPI_TAG)
        {
        // aktualizacja (wszystkie wez/oddaj) | (czy dobrze dałem synchro zegara?)
        case WEZ_POJAZD:
            pthread_mutex_lock(&vehicles_mut);
            vehicles--;
            vehicle_table[pakiet.data].key = 0;
            S_poss = vec_poss(vehicle_list, pakiet.dst);
            vehicle_list.erase(vehicle_list.begin() + S_poss);
            pthread_mutex_unlock(&vehicles_mut);
            //Lclock = max(Lclock, pakiet.ts);
            break;
        case ODDAJ_POJAZD:
            pthread_mutex_lock(&vehicles_mut);
            vehicles++;
            vehicle_table[pakiet.data].key = 1;
            vehicle_table[pakiet.data].value--;
            if (vehicle_table[pakiet.data].value == 0)
            {
                vehicle_table[pakiet.data].value = max_state;
            }
            pthread_mutex_unlock(&vehicles_mut);
            //Lclock = max(Lclock, pakiet.ts);
            break;
        case WEZ_MECH:
            pthread_mutex_lock(&mechanics_mut);
            mechanics--;
            S_poss = vec_poss(mechanic_list, pakiet.dst);
            mechanic_list.erase(mechanic_list.begin() + S_poss);
            pthread_mutex_unlock(&mechanics_mut);
            //Lclock = max(Lclock, pakiet.ts);
            break;
        case ODDAJ_MECH:
            pthread_mutex_lock(&mechanics_mut);
            mechanics++;
            pthread_mutex_unlock(&mechanics_mut);
            //Lclock = max(Lclock, pakiet.ts);
            break;
        case WEZ_MORZE:
            pthread_mutex_lock(&sea_mut);
            space_on_sea = space_on_sea - pakiet.data;
            S_poss = vec_poss(sea_list, pakiet.dst);
            sea_list.erase(sea_list.begin() + S_poss);
            pthread_mutex_unlock(&sea_mut);
            //Lclock = max(Lclock, pakiet.ts);
            break;

        case ODDAJ_MORZE:
            pthread_mutex_lock(&sea_mut);
            space_on_sea = space_on_sea + pakiet.data;
            pthread_mutex_unlock(&sea_mut);
            //Lclock = max(Lclock, pakiet.ts);
            break;

        case KOLEJKA_POJAZD:
            pthread_mutex_lock(&vehicles_mut);
            vehicle_list=add_too_vec(vehicle_list, pakiet.dst, pakiet.ts);
            if (list_c == 'V')
            {
                poss = vec_poss(vehicle_list, ranking);
            }
            pakiet.ts = Lclock;
            MPI_Send(&pakiet, 1, MPI_PAKIET_T, pakiet.dst, OK, MPI_COMM_WORLD);
            //Lclock = max(Lclock, pakiet.ts);
            pthread_mutex_unlock(&vehicles_mut);
            break;
        case KOLEJKA_MORZE: // aktualizacja
            pthread_mutex_lock(&sea_mut);
            sea_list=add_too_vec(sea_list, pakiet.dst, pakiet.ts);
            if (list_c == 'S')
            {
                poss = vec_poss(sea_list, ranking);
            }
            pakiet.ts = Lclock;
            MPI_Send(&pakiet, 1, MPI_PAKIET_T, pakiet.dst, OK, MPI_COMM_WORLD);
            //Lclock = max(Lclock, pakiet.ts);
            pthread_mutex_unlock(&sea_mut);

            break;
        case KOLEJKA_MECH: // aktualizacja
            pthread_mutex_lock(&mechanics_mut);
            mechanic_list=add_too_vec(mechanic_list, pakiet.dst, pakiet.ts);
            if (list_c == 'M')
            {
                poss = vec_poss(mechanic_list, ranking);
            }

            pakiet.ts = Lclock;
            MPI_Send(&pakiet, 1, MPI_PAKIET_T, pakiet.dst, OK, MPI_COMM_WORLD);
            //Lclock = max(Lclock, pakiet.ts);
            pthread_mutex_unlock(&mechanics_mut);

            break;
        case OK:
            pthread_mutex_lock(&ok_mut);
            ok += 1;
            //Lclock = max(Lclock, pakiet.ts);
            pthread_mutex_unlock(&ok_mut);
            break;
        default:
            printf("Unexpected massage!\n");
            break;
        }
    }
    printf(" Koniec! RECV");
    return 0;
}

void *mainFunc(void *ptr)
{
    packet_t pakiet;
    pakiet.src = ranking;
    int x;
    //int max_space = space_on_sea / vehicles * 2;
    int max_space = 2;
    if (max_space > space_on_sea)
        max_space = space_on_sea;
    bool take;
    unsigned int k;

    while (!ending)
    {
        printf("RANK[%d] Zegar[%d] - zbiera załoge\n", ranking, Lclock);
        wait();
        x = 2;
        printf("RANK[%d] Zegar[%d] zebral [%d]\n", ranking,Lclock, x);

        /*------------------------------------VECHICLES TAKE------------------------------------------------------*/
        //Lclock += 1;
        pthread_mutex_lock(&ok_mut);
        ok = 1;
        pthread_mutex_unlock(&ok_mut);
        clock_max(-1);
        pakiet.ts = Lclock;
        printf("RANK[%d] Zegar[%d] - Probuje wziac pojazd\n", ranking, Lclock);
        pthread_mutex_lock(&vehicles_mut);
        list_c = 'V';
        for (int i = 0; i < size; i++)
        {
            if (i == ranking)
            {
                continue;
            }
            pakiet.dst = i;
            MPI_Send(&pakiet, 1, MPI_PAKIET_T, pakiet.dst, KOLEJKA_POJAZD, MPI_COMM_WORLD);
        }
        vehicle_list=add_too_vec(vehicle_list, ranking, Lclock);
        poss = vec_poss(vehicle_list, ranking);
        pthread_mutex_unlock(&vehicles_mut);
        while (ok < size)
            ;
        take = true;
        do
        {
            printf("RANK[%d] Zegar[%d] - Czeka na pojazd (JEST: %d)\n", ranking, Lclock, vehicles);
            pthread_mutex_lock(&vehicles_mut);
            poss = vec_poss(vehicle_list, ranking);
            if (vehicles > poss)
            {
                
                vehicles--;

                k = 0;
                int T = -1;
                for (unsigned int i = 0; i < vehicle_table.size(); ++i)
                {
                    if (vehicle_table[i].key == 1)
                    {
                        T++;
                    }
                    if (T == poss)
                    {
                        k = i;
                        T++;
                    }
                }
                
                vehicle_table[k].key = 0;
                poss = vec_poss(vehicle_list, ranking);
                vehicle_list.erase(vehicle_list.begin() + poss);
                take = false;
                pakiet.data = k;
                clock_max(-1);
                pakiet.ts = Lclock;
                for (int i = 0; i < size; i++)
                {
                    if (i == ranking)
                    {
                        continue;
                    }
                    pakiet.dst = i;

                    MPI_Send(&pakiet, 1, MPI_PAKIET_T, pakiet.dst, WEZ_POJAZD, MPI_COMM_WORLD);
                }
                list_c = '0';
                printf("RANK[%d] Zegar[%d] - Bierze pojazd (ID:  %d STAN: %d)\n", ranking, Lclock ,k, vehicle_table[k].value);
            }
            
            pthread_mutex_unlock(&vehicles_mut);
            wait();
        } while (take);

        /*------------------------------------SEA TAKE---------------------------------------------------------------*/
        // aktualizacja mutexy

        clock_max(-1);
        pakiet.ts = Lclock;
        pakiet.data = x;
        printf("RANK[%d] Zegar[%d] - Probuje wziac morze\n", ranking, Lclock);
        pthread_mutex_lock(&ok_mut);
        ok = 1;
        pthread_mutex_unlock(&ok_mut);
        pthread_mutex_lock(&sea_mut);
        list_c = 'S';
        for (int i = 0; i < size; i++)
        {
            if (i == ranking)
            {
                continue;
            }
            pakiet.dst = i;

            MPI_Send(&pakiet, 1, MPI_PAKIET_T, pakiet.dst, KOLEJKA_MORZE, MPI_COMM_WORLD);
        }
        sea_list=add_too_vec(sea_list, ranking, Lclock);
        poss = vec_poss(sea_list, ranking);
        pthread_mutex_unlock(&sea_mut);
        while (ok < size)
            ;

        take = true;
        do
        {
            
            printf("RANK[%d] Zegar[%d] - Czeka na morze(MA: %d JEST: %d)\n", ranking, Lclock,x,space_on_sea);
            pthread_mutex_lock(&sea_mut);
            poss = vec_poss(sea_list, ranking);
            if ((poss * max_space + x) <= space_on_sea)
            {
                space_on_sea = space_on_sea - x;
                poss = vec_poss(sea_list, ranking);
                sea_list.erase(sea_list.begin() + poss);
                take = false;
                clock_max(-1);
                pakiet.ts = Lclock;
                pakiet.data = x;
                
                for (int i = 0; i < size; i++)
                {
                    if (i == ranking)
                    {
                        continue;
                    }
                    pakiet.dst = i;

                    MPI_Send(&pakiet, 1, MPI_PAKIET_T, pakiet.dst, WEZ_MORZE, MPI_COMM_WORLD);
                }
                list_c = '0';
                printf("RANK[%d] Zegar[%d] - Bierze morze(ZABRAL: %d ZOSTALO: %d)\n", ranking, Lclock,x,space_on_sea);
            }
            
            pthread_mutex_unlock(&sea_mut);
            wait();
        } while (take);

        /*------------------------------------TRIP---------------------------------------------------------------------*/
        printf("RANK[%d] Zegar[%d] - Płynie\n", ranking, Lclock);
        wait();
        pthread_mutex_lock(&vehicles_mut); // aktualizacja
        vehicle_table[k].value--;
        pthread_mutex_unlock(&vehicles_mut);

        /*------------------------------------SEA RETURN---------------------------------------------------------------*/
        clock_max(-1);
        pakiet.ts = Lclock;
        pakiet.data = x;
        
        for (int i = 0; i < size; i++)
        {
            if (i == ranking)
            {
                continue;
            }
            pakiet.dst = i;

            MPI_Send(&pakiet, 1, MPI_PAKIET_T, pakiet.dst, ODDAJ_MORZE, MPI_COMM_WORLD);
        }
        pthread_mutex_lock(&sea_mut);
        space_on_sea = space_on_sea + x;
        printf("RANK[%d] Zegar[%d] - Zwraca morze(ODDAL: %d ZOSTALO: %d)\n", ranking, Lclock,x,space_on_sea);
        pthread_mutex_unlock(&sea_mut);
        wait();

        if (vehicle_table[k].value == 0)
        {
            /*--------------------------------MECHANICS TAKE-----------------------------------------------------------*/
            // aktualizacja mutexy
            clock_max(-1);
            pakiet.ts = Lclock;
            pthread_mutex_lock(&ok_mut);
            ok = 1;
            pthread_mutex_unlock(&ok_mut);
            pthread_mutex_lock(&mechanics_mut);
            list_c = 'M';
            for (int i = 0; i < size; i++)
            {
                if (i == ranking)
                {
                    continue;
                }
                pakiet.dst = i;

                MPI_Send(&pakiet, 1, MPI_PAKIET_T, pakiet.dst, KOLEJKA_MECH, MPI_COMM_WORLD);
            }
            mechanic_list=add_too_vec(mechanic_list, ranking, Lclock);
            poss = vec_poss(mechanic_list, ranking);
            pthread_mutex_unlock(&mechanics_mut);

            while (ok < size)
                ;

            take = true;
            do
            {
                printf("RANK[%d] Zegar[%d] - Czeka na mechanika(JEST: %d)\n", ranking, Lclock,mechanics);
                pthread_mutex_lock(&mechanics_mut);
                 poss = vec_poss(mechanic_list, ranking);
                if (mechanics > poss)
                {
                    mechanics--;
                     poss = vec_poss(mechanic_list, ranking);
                    mechanic_list.erase(mechanic_list.begin() + poss);
                    take = false;
                    clock_max(-1);
                    pakiet.ts = Lclock;
                    for (int i = 0; i < size; i++)
                    {
                        if (i == ranking)
                        {
                            continue;
                        }
                        pakiet.dst = i;

                        MPI_Send(&pakiet, 1, MPI_PAKIET_T, pakiet.dst, WEZ_MECH, MPI_COMM_WORLD);
                    }
                    list_c = '0';
                    printf("RANK[%d] Zegar[%d] - Bierze mechanika(ZOSTALO : %d)\n", ranking, Lclock,mechanics);
                }
               
                pthread_mutex_unlock(&mechanics_mut);
                wait();
            } while (take);

            /*--------------------------------MECHANICS REPAIR----------------------------------------------------------*/
            printf("RANK[%d] Zegar[%d] - naprawa auta o ID: %d \n", ranking, Lclock,k);
            pthread_mutex_lock(&vehicles_mut); // aktualizacja
            vehicle_table[k].value = max_state;
            pthread_mutex_unlock(&vehicles_mut);
            wait();

            /*--------------------------------MECHANICS RETURN----------------------------------------------------------*/
            clock_max(-1);
            pakiet.ts = Lclock;
            
            for (int i = 0; i < size; i++)
            {
                if (i == ranking)
                {
                    continue;
                }
                pakiet.dst = i;

                MPI_Send(&pakiet, 1, MPI_PAKIET_T, pakiet.dst, ODDAJ_MECH, MPI_COMM_WORLD);
            }
            pthread_mutex_lock(&mechanics_mut);
            mechanics++;
            printf("RANK[%d] Zegar[%d] - oddaje mechanika(ZOSTALO : %d)\n", ranking, Lclock,mechanics);
            pthread_mutex_unlock(&mechanics_mut);
            wait();
        }

        /*------------------------------------VEHICLES RETURN-----------------------------------------------------------*/
        clock_max(-1);
        pakiet.ts = Lclock;
        pakiet.data = k;
        
        for (int i = 0; i < size; i++)
        {
            if (i == ranking)
            {
                continue;
            }
            pakiet.dst = i;

            MPI_Send(&pakiet, 1, MPI_PAKIET_T, pakiet.dst, ODDAJ_POJAZD, MPI_COMM_WORLD);
        }
        pthread_mutex_lock(&vehicles_mut);
        vehicle_table[k].key=1;
        vehicles++;
        printf("RANK[%d] Zegar[%d] - Oddaje pojazd(ID:  %d STAN: %d)\n", ranking, Lclock, k,vehicle_table[k].value);
        pthread_mutex_unlock(&vehicles_mut);
        
        wait();
    }

    printf(" Koniec! MAIN\n");
    return 0;
}

int main(int argc, char *argv[])
{
    inicjuj(&argc, &argv);

    while (!ending)
        ;

    MPI_Finalize();
}
